<?php

namespace {Vendor}\{Module}\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class CreateCustomAttribute implements DataPatchInterface
{
    private $moduleDataSetup;
    private $eavSetupFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'custom_attribute_code',    
            [
                'type' => 'varchar',
                'label' => 'Custom Attribute Label',
                'input' => 'text',
                'frontend' => '',
                'used_in_product_listing' => false,
                'group' => 'General',
                'required' => true,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible_on_front' => false,
                'unique' => false,
                'is_used_in_grid' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'default' => '43119',
            ]
        );

        $this->moduleDataSetup->endSetup();
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [];
    }
}